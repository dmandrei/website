### configs:
- [jupyter lab shortcuts](jupyter)
- [vimrc](vimrc)
- [ranger](ranger)
- [tmux](tmux)
- [misc linux tips](misc)
