```
# tmux does not change colorscheme
set-option -sa terminal-overrides ",xterm*:Tc"
# remap prefix to Control + a
set -g prefix C-a
# bind 'C-a C-a' to type 'C-a'
bind C-a send-prefix
unbind C-b
setw -g window-active-style 'bg=grey fg=yellow,bold'
# force a reload of the config file
unbind r
bind r source-file ~/.tmux.conf
# quick pane cycling
unbind ^A
bind ^A select-pane -t :.+
```
