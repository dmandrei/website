```
set nocompatible
set number
set cursorline
set ignorecase
set history=1000
set wildmenu
set guifont=Menlo:h34
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent
set expandtab "don't use actual tab character (ctrl-v)"
filetype on
syntax on
map <F1> <Esc>
imap <F1> <Esc>
nnoremap q: <nop>
nnoremap Q <nop>
```
