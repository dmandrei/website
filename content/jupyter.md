#### jupyter shortcuts:
```
"shortcuts": [
        {
            "args": {},
            "command": "notebook:move-cursor-down",
            "keys": [
                "J"
            ],
            "selector": ".jp-Notebook:focus"
        },
        {
            "args": {},
            "command": "notebook:move-cursor-down",
            "keys": [
                "ArrowDown"
            ],
            "selector": ".jp-Notebook:focus",
            "disabled":true
        },
        {
            "args": {},
            "command": "notebook:move-cursor-up",
            "keys": [
                "ArrowUp"
            ],
            "selector": ".jp-Notebook:focus",
            "disabled": true
        },
        {
            "args": {},
            "command": "notebook:move-cursor-up",
            "keys": [
                "K"
            ],
            "selector": ".jp-Notebook:focus"
        },
        {
            "args": {},
            "command": "application:activate-next-tab",
            "keys": [
                "Ctrl Shift ]"
            ],
            "selector": "body"
        },
        {
            "args": {},
            "command": "application:activate-previous-tab",
            "keys": [
                "Ctrl Shift ["
            ],
            "selector": "body"
        },
        {
            "args": {},
            "command": "application:close",
            "keys": [
                "Shift Ctrl W"
            ],
            "selector": ".jp-Activity"
        },]
```
